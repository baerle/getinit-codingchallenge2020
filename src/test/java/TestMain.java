import java.util.ArrayList;
import java.util.List;

public class TestMain {

    public static void main(String[] args) {
        // Initialisation
        List<Subsidiary> subsidiaries = new ArrayList<>(4);
        subsidiaries.add(new Subsidiary(1, "Location", "Street", "HouseNr", 97865, "City", 0, 0));
        subsidiaries.add(new Subsidiary(2, "Location", "Street", "HouseNr", 97865, "City", 2, 0));
        subsidiaries.add(new Subsidiary(3, "Location", "Street", "HouseNr", 97865, "City", 0, 2));
        subsidiaries.add(new Subsidiary(4, "Location", "Street", "HouseNr", 97865, "City", 2, 2));
        subsidiaries.add(new Subsidiary(5, "Location", "Street", "HouseNr", 97865, "City", 1, 2));
        subsidiaries.add(new Subsidiary(6, "Location", "Street", "HouseNr", 97865, "City", -0.8, -2));


        RoundtripFinder rf = new RoundtripFinder(subsidiaries, 0);
        for (Subsidiary i : rf.getPath()) {
            System.out.println(i);
        }
        rf.printTotalLength();


        RoundtripFinderImproved rfi = new RoundtripFinderImproved(subsidiaries, subsidiaries.get(0));
        for (Subsidiary s : rfi.getPath()) {
            System.out.println(s);
        }
        rfi.printTotalLength();


        RoundtripFinderBranchAndBound rfbab = new RoundtripFinderBranchAndBound(subsidiaries, subsidiaries.get(0));
        for (int s : rfbab.getPath()) {
            System.out.println(subsidiaries.get(s));
        }
        double minimumDistance = rfbab.getLength();
        System.out.printf("Minimum cost : %.2f\n", minimumDistance);


        double sum = 0;
        sum += subsidiaries.get(0).getDistanceTo(subsidiaries.get(5));
        sum += subsidiaries.get(5).getDistanceTo(subsidiaries.get(1));
        sum += subsidiaries.get(1).getDistanceTo(subsidiaries.get(3));
        sum += subsidiaries.get(3).getDistanceTo(subsidiaries.get(4));
        sum += subsidiaries.get(4).getDistanceTo(subsidiaries.get(2));
        sum += subsidiaries.get(2).getDistanceTo(subsidiaries.get(0));
        System.out.println("smallest logical length: " + sum);  // 1 6 2 4 5 3 1
    }
}
