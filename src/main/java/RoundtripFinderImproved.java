import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class RoundtripFinderImproved {
    List<Subsidiary> subsidiaries;
    LinkedList<Subsidiary> path;
    Subsidiary startSubsidiary;
    double latitudeCenter, longitudeCenter;
    boolean[] left;
    List<Subsidiary> newLeftSubsidiaries;
    List<Subsidiary> newRightSubsidiaries;

    public RoundtripFinderImproved(List<Subsidiary> subsidiaries, Subsidiary startSubsidiary) {
        if (subsidiaries.size() > 0)
            this.subsidiaries = subsidiaries;
        else throw new IllegalArgumentException("no subsidiaries in subsidiaries list given");
        this.startSubsidiary = startSubsidiary;
        this.left = new boolean[subsidiaries.size()];
        this.newLeftSubsidiaries = new ArrayList<>(this.subsidiaries.size());
        this.newRightSubsidiaries = new ArrayList<>(this.subsidiaries.size());

        this.path = new LinkedList<>();

        double[] gravityCenter = RoundtripFinderUtilities.calculateCenterOfGravity(this.subsidiaries);
        this.longitudeCenter = gravityCenter[0];
        this.latitudeCenter = gravityCenter[1];

        this.findWay();
    }

    /**
     * works recursively
     * TODO: describe more
     */
    public void findWay() {
        if (this.subsidiaries.size() > 2) {
            // split
            this.splitInLeftAndRight();
            RoundtripFinderImproved left = new RoundtripFinderImproved(this.newLeftSubsidiaries, this.startSubsidiary);
            Subsidiary newStart = left.getPath().getLast();
            this.newRightSubsidiaries.add(newStart);
            if (this.newRightSubsidiaries.size() > 0) {
                RoundtripFinderImproved right = new RoundtripFinderImproved(this.newRightSubsidiaries, newStart);
                // concat
                concatWays(left.getPath(), right.getPath());
            } else {
                this.path.addAll(left.getPath());
            }
        } else {
            this.path.add(this.startSubsidiary);
            if (this.subsidiaries.size() == 2) {
                this.path.add(this.subsidiaries.get(this.subsidiaries.size() - 1 - this.subsidiaries.indexOf(this.startSubsidiary)));
            }/* else {    // this.subsidiaries.size() == 3
                int indexStart = this.subsidiaries.indexOf(this.startSubsidiary);
                double minWayLength = Double.MAX_VALUE;
                for (int i = 0; i < 3; i++) {
                    if (i != indexStart) {  // i is the second step of the path
                        int thirdIndex = 3 - indexStart - i;
                        double wayLength = this.startSubsidiary.getDistanceTo(this.subsidiaries.get(i));
                        wayLength += this.subsidiaries.get(i).getDistanceTo(this.subsidiaries.get(thirdIndex));

                        //find minimum way
                        if (wayLength < minWayLength) {
                            minWayLength = wayLength;
                            this.path.add(1, this.subsidiaries.get(i));
                            this.path.add(2, this.subsidiaries.get(thirdIndex));
                        }
                    }
                }
                this.path = new LinkedList<>(this.path.subList(0, 3));
            }*/
        }
    }

    /**
     * concatenates the two paths at subsidiary both paths containing (last item in left and first item in right path)
     *
     * @param left  LinkedList<Subsidiary>
     * @param right LinkedList<Subsidiary>
     */
    public void concatWays(LinkedList<Subsidiary> left, LinkedList<Subsidiary> right) {
        this.path.addAll(left);
        for (int i = 1; i < right.size(); i++) {
            this.path.add(right.get(i));
        }
    }

    /**
     * calculates a straight from startSubsidiary through the geometric center of the left graph
     * adds the subsidiaries to the associated new subsidiary list
     */
    private void splitInLeftAndRight() {
        double m = (this.latitudeCenter - this.startSubsidiary.getLatitude())
                / (this.longitudeCenter - this.startSubsidiary.getLongitude()); //latitude = y | longitude = x ||  slope
        double t = this.startSubsidiary.getLatitude() - (m * this.startSubsidiary.getLongitude());    //move in y-axis  t = y - m * x
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("m:" + m);
            System.out.println("t:" + t);
        }
        for (int i = 0; i < this.subsidiaries.size(); i++) {
            this.left[i] = (this.subsidiaries.get(i).getLatitude() >= Math.nextDown(m * this.subsidiaries.get(i).getLongitude() + t));
            if (this.left[i]) {
                this.newLeftSubsidiaries.add(this.subsidiaries.get(i));
            } else {
                this.newRightSubsidiaries.add(this.subsidiaries.get(i));
            }
        }
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("split array: " + Arrays.toString(this.left));
        }
    }

    /**
     * this is the result path
     *
     * @return LinkedList<Subsidiary>
     */
    public LinkedList<Subsidiary> getPath() {
        return path;
    }

    public void printTotalLength() {
        System.out.println("\nTotal length: " + RoundtripFinderUtilities.length(this.path));
    }

    public double getLength() {
        return RoundtripFinderUtilities.length(this.path);
    }
}
