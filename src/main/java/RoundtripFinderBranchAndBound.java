import java.util.Arrays;
import java.util.List;

/**
 * a modified version of https://www.geeksforgeeks.org/traveling-salesman-problem-using-branch-and-bound-2/
 */
public class RoundtripFinderBranchAndBound {
    private int size;
    private double[][] adjacency;
    private int[] finalPath;
    private boolean[] visited;  //Nodes already visited in path
    private double minimumDistance = Double.MAX_VALUE;

    public RoundtripFinderBranchAndBound(double[][] adjacency, int startIndex) {
        this.size = adjacency.length;
        this.adjacency = adjacency;
        this.finalPath = new int[this.size + 1];
        this.visited = new boolean[this.size];

        this.findRoundtrip(startIndex);
    }

    public RoundtripFinderBranchAndBound(List<Subsidiary> subsidiaries, Subsidiary startSubsidiary) {
        this.size = subsidiaries.size();
        this.finalPath = new int[this.size + 1];
        this.visited = new boolean[this.size];

        this.adjacency = new double[this.size][this.size];
        for (int i = 0; i < subsidiaries.size(); i++) {
            for (int j = 0; j < subsidiaries.size(); j++) {
                this.adjacency[i][j] = subsidiaries.get(i).getDistanceTo(subsidiaries.get(j));
            }
        }

        this.findRoundtrip(subsidiaries.indexOf(startSubsidiary));
    }

    /**
     * Call this method to fill the finalPath array with the best possible result
     * @param startSubsidiary: int --> index of start node
     */
    public void findRoundtrip(int startSubsidiary) {
        // Initialize the currentPath and visited array
        int[] currentPath = new int[this.size + 1];
        Arrays.fill(currentPath, -1);
        Arrays.fill(this.visited, false);

        // Calculate initial lower bound for the root node
        // using the formula 1/2 * (sum of first min +
        // second min) for all edges.
        double currentBound = 0;
        for (int i = 0; i < this.size; i++)
            currentBound += (firstMin(i) + secondMin(i));
        currentBound /= 2;

        // Set startSubsidiary as first visited in currentPath
        visited[startSubsidiary] = true;
        currentPath[0] = startSubsidiary;

        branchAndBound(currentBound, 0, 1, currentPath);
    }

    /**
     * @param currentBound -> lower bound of the root node
     * @param currentDistance -> stores the weight of the path so far
     * @param level-> current level while moving in the search space tree
     * @param currentPath -> where the solution is being stored which would later be copied to the final_path array
     */
    private void branchAndBound(double currentBound, double currentDistance, int level, int[] currentPath) {
        // end case
        if (level == this.size) {
            currentDistance += this.adjacency[currentPath[level - 1]][currentPath[0]];

            if (currentDistance < this.minimumDistance) {
                copyToFinalPath(currentPath);
                this.minimumDistance = currentDistance;
            }
            return;
        }

        // for any other level iterate for all vertices to
        // build the search space tree recursively
        for (int i = 0; i < this.size; i++) {
            // Consider next vertex if it is not same (diagonal entry in adjacency matrix and not already visited)
            if (this.adjacency[currentPath[level - 1]][i] != 0 && !visited[i]) {
                double temp = currentBound;
                currentDistance += this.adjacency[currentPath[level - 1]][i];

                // different computation of currentBound for level 1 from the other levels
                if (level == 1) {
                    currentBound -= ((firstMin(currentPath[level - 1]) +
                            firstMin(i)) / 2);
                } else {
                    currentBound -= ((secondMin(currentPath[level - 1]) +
                            firstMin(i)) / 2);
                }

                // currentBound + currentDistance is the actual lower bound for the current node
                double currentLowerBound = currentBound + currentDistance;

                // If current lower bound < final_res, further explore the node
                if (currentLowerBound < this.minimumDistance) {
                    currentPath[level] = i;
                    this.visited[i] = true;

                    // call TSPRec for the next level
                    branchAndBound(currentBound, currentDistance, level + 1, currentPath);
                }

                // Else we have to prune the node by resetting
                // all changes to currentDistance and currentBound
                currentDistance -= this.adjacency[currentPath[level - 1]][i];
                currentBound = temp;

                // Also reset the visited array
                Arrays.fill(this.visited, false);
                for (int j = 0; j <= level - 1; j++)
                    this.visited[currentPath[j]] = true;
            }
        }
    }

    /**
     * function to find the minimum length from node to another node
     * @param nodeIndex -> node index from which edge starts
     */
    private double firstMin(int nodeIndex) {
        double min = Double.MAX_VALUE;
        for (int k = 0; k < this.size; k++)
            if (this.adjacency[nodeIndex][k] < min && nodeIndex != k)
                min = this.adjacency[nodeIndex][k];
        return min;
    }

    /**
     * function to find the second minimum length from node to another node
     * @param nodeIndex -> node index from which edge starts
     */
    private double secondMin(int nodeIndex) {
        double first = Double.MAX_VALUE, second = Double.MAX_VALUE;
        for (int j = 0; j < this.size; j++) {
            if (nodeIndex == j)
                continue;

            if (this.adjacency[nodeIndex][j] <= first) {
                second = first;
                first = this.adjacency[nodeIndex][j];
            } else if (this.adjacency[nodeIndex][j] <= second && this.adjacency[nodeIndex][j] != first)
                second = this.adjacency[nodeIndex][j];
        }
        return second;
    }

    /**
     * copies the current path to final path and setting the starting node at last node on final path
     * @param currentPath: int[] -> current path to copy to final path
     */
    private void copyToFinalPath(int[] currentPath) {
        for (int i = 0; i < this.size; i++)
            this.finalPath[i] = currentPath[i];
        this.finalPath[this.size] = currentPath[0];
    }

    public double getLength() {
        return this.minimumDistance;
    }

    public int[] getPath() {
        return this.finalPath;
    }

    // Test code
    public static void main(String[] args) {
        //Adjacency matrix for the given graph
        double[][] adj = {
                {0, 10, 15, 20},
                {10, 0, 35, 25},
                {15, 35, 0, 30},
                {20, 25, 30, 0}
        };

        RoundtripFinderBranchAndBound rf = new RoundtripFinderBranchAndBound(adj, 0);

        System.out.printf("Minimum cost : %.2f\n", rf.getLength());
        System.out.print("Path Taken : ");
        for (int i : rf.getPath()) {
            System.out.printf("%d ", i);
        }
    }
}
