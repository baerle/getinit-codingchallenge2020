import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileInputReader {
    String[] keys;
    int[] keyOrder = new int[Subsidiary.keys.length];
    List<Subsidiary> subsidiaries;

    public FileInputReader(File file) {
        this.subsidiaries = new ArrayList<>(25);    // slightly bigger than the actual number of subsidiaries (21) to prevent performance loss for copying
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            boolean first = true;
            String dataLine;
            while ((dataLine = br.readLine()) != null) {
                String[] subsidiary = dataLine.split(",");
                if (first) {
                    first = false;
                    this.keys = subsidiary;
                    findOrderOfKeys();
                } else {
                    this.subsidiaries.add(new Subsidiary(
                            Integer.parseInt(subsidiary[this.keyOrder[0]]),
                            subsidiary[this.keyOrder[1]],
                            subsidiary[this.keyOrder[2]],
                            subsidiary[this.keyOrder[3]],
                            Integer.parseInt(subsidiary[this.keyOrder[4]]),
                            subsidiary[this.keyOrder[5]],
                            Double.parseDouble(subsidiary[this.keyOrder[6]]),
                            Double.parseDouble(subsidiary[this.keyOrder[7]])
                    ));
                }
            }
        } catch (IOException e) {
            System.out.println("Couldn't find/read/parse the file: " + file);
            System.out.println(e.getMessage());
        }
    }

    private void findOrderOfKeys() {
        for (int i = 0; i < this.keys.length; i++) {
            for (String[] keyIndex : Subsidiary.keys) {
                if (keyIndex[0].equals(this.keys[i])) {
                    this.keyOrder[i] = Integer.parseInt(keyIndex[1]);
                    break;
                }
            }
        }
    }

    public List<Subsidiary> getSubsidiaryList() {
        return this.subsidiaries;
    }
}
