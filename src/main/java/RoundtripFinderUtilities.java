import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class RoundtripFinderUtilities {

    /**
     * Returns an array of two doubles.
     * First is the longitude center of gravity.
     * Second is the latitude center of gravity.
     * @param subsidiaries: List<Subsidiary>
     * @return center of gravity: double[]
     */
    public static double[] calculateCenterOfGravity(List<Subsidiary> subsidiaries) {
        double longitudeSum = 0.0;
        double latitudeSum = 0.0;

        for (Subsidiary subsidiary : subsidiaries) {
            longitudeSum += subsidiary.getLongitude();
            latitudeSum += subsidiary.getLatitude();
        }

        double[] result = new double[2];
        result[0] = longitudeSum / subsidiaries.size();   //longitudeCenter
        result[1] = latitudeSum / subsidiaries.size();    //latitudeCenter
        return result;
    }

    public static boolean debug() {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader("config.env"));
        } catch (IOException e) {
            System.out.println("Couldn't read config.env");
        }
        if (System.getenv("debug") != null)
            return System.getenv("debug").equals("true");
        if (properties.get("debug") != null) {
            return properties.get("debug").equals("true");
        }
        return false;
    }

    public static double length(List<Subsidiary> subsidiaries) {
        double sum = 0;
        Subsidiary last = subsidiaries.get(0);
        for (Subsidiary i : subsidiaries) {
            sum += i.getDistanceTo(last);
            last = i;
        }
        return sum;
    }
}
