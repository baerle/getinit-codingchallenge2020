import java.io.File;
import java.util.List;

public class Main {
    public static final String FILE_PATH = "src/main/resources/msg_standorte_deutschland.csv";

    public static void main(String[] args) {
        FileInputReader fileInputReader = new FileInputReader(new File(FILE_PATH));
        List<Subsidiary> subsidiaries = fileInputReader.getSubsidiaryList();

        System.out.println("Starting calculation - the execution can endure up to 3 minutes, depending on your machine. Please stay patient");

        RoundtripFinder rf = new RoundtripFinder(subsidiaries, 0);
        double rfLength = rf.getLength();

        RoundtripFinderImproved rfi = new RoundtripFinderImproved(subsidiaries, subsidiaries.get(0));
        double rfiLength = rfi.getLength();

        Stopwatch stopwatch = new Stopwatch();
        RoundtripFinderBranchAndBound rfbab = new RoundtripFinderBranchAndBound(subsidiaries, subsidiaries.get(0));
        double rfbabLength = rfbab.getLength();
        System.out.println("needed " + stopwatch + " to calculate best result");

        if (rfbabLength < rfiLength) {
            if (rfbabLength < rfLength) {
                System.out.printf("\nShortest route (Branch and Bound): %.2f\n\n", rfbabLength);

                for (int s : rfbab.getPath()) {
                    System.out.println(subsidiaries.get(s));
                }
            } else {
                System.out.printf("\nShortest route (RoundtripFinder): %.2f\n\n", rfLength);
            }
        } else {
            if (rfiLength < rfLength) {
                System.out.printf("\nShortest route (RoundtripFinderImproved): %.2f\n\n", rfiLength);
            } else {
                System.out.printf("\nShortest route (RoundtripFinder): %.2f\n\n", rfLength);
            }
        }
    }
}
