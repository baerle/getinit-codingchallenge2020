import java.util.*;

public class RoundtripFinder {
    public static final int ANGLE_DIFFERENCE = 45;
    List<Subsidiary> subsidiaries;
    double[] distance, direction;
    ArrayList<Subsidiary> previous;
    double latitudeCenter, longitudeCenter;
    boolean[] left;
    int startSubsidiary;
    int countSubsidiaries;
    List<Subsidiary> newLeftSubsidiaries;
    List<Subsidiary> newRightSubsidiaries;

    public RoundtripFinder(List<Subsidiary> subsidiaries, int startSubsidiary) {
        this.subsidiaries = subsidiaries;
        this.distance = new double[subsidiaries.size()];
        this.direction = new double[subsidiaries.size()];
        this.previous = new ArrayList<>(subsidiaries.size() + 1);
        this.previous.add(subsidiaries.get(startSubsidiary));
        this.left = new boolean[subsidiaries.size()];
        this.startSubsidiary = startSubsidiary;
        this.countSubsidiaries = 0;
        newLeftSubsidiaries = new ArrayList<>(this.subsidiaries.size());
        newRightSubsidiaries = new ArrayList<>(this.subsidiaries.size());

        double[] gravityCenter = RoundtripFinderUtilities.calculateCenterOfGravity(this.subsidiaries);
        this.longitudeCenter = gravityCenter[0];
        this.latitudeCenter = gravityCenter[1];
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("center latitude: " + latitudeCenter);
            System.out.println("center longitude: " + longitudeCenter);
        }
        this.recursiveSplitAndMerge();
    }

    private void recursiveSplitAndMerge() { //TODO: refactor to "merge-sort like"
        if (this.subsidiaries.size() > 2) {
            if (RoundtripFinderUtilities.debug()) {
                System.out.println("\nSubsidiary-Size: " + this.subsidiaries.size() + "\n");
            }
            this.splitInLeftAndRight();
//            RoundtripFinder left = new RoundtripFinder(this.newLeftSubsidiaries, this.startSubsidiary);
        } else {
            if (RoundtripFinderUtilities.debug()) {
                System.out.println("no further splitting");
                System.out.println(this.subsidiaries);
            }
        }
        int lastSubsidiary = this.shortestDistance2(true, startSubsidiary);   //left route
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("left subsidiary: " + lastSubsidiary);
        }

        lastSubsidiary = this.shortestDistance2(false, lastSubsidiary);   //right route
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("right subsidiary: " + lastSubsidiary);
        }

        if (this.newRightSubsidiaries.size() > 2) {
            if (RoundtripFinderUtilities.debug()) {
                System.out.println("right split");
            }
//            RoundtripFinder right = new RoundtripFinder(this.newRightSubsidiaries, this.newRightSubsidiaries.indexOf(this.subsidiaries.get(lastSubsidiary)));
        }
    }

    /**
     * calculates a straight from startSubsidiary through the geometric center of the left graph
     * adds the subsidiaries to the associated new subsidiary list
     */
    private void splitInLeftAndRight() {
        double m = (this.latitudeCenter - this.subsidiaries.get(this.startSubsidiary).getLatitude())
                / (this.longitudeCenter - this.subsidiaries.get(this.startSubsidiary).getLongitude()); //latitude = y | longitude = x ||  slope
        double t = this.subsidiaries.get(this.startSubsidiary).getLatitude() - (m * this.subsidiaries.get(this.startSubsidiary).getLongitude());    //move in y-axis
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("m:" + m);
            System.out.println("t:" + t);
        }
        for (int i = 0; i < this.subsidiaries.size(); i++) {
            if (i != this.startSubsidiary) {
                this.left[i] = (this.subsidiaries.get(i).getLatitude() >= m * this.subsidiaries.get(i).getLongitude() + t);
                if (this.left[i]) {
                    this.newLeftSubsidiaries.add(this.subsidiaries.get(i));
                } else {
                    this.newRightSubsidiaries.add(this.subsidiaries.get(i));
                }
            }
        }
        if (RoundtripFinderUtilities.debug()) {
            System.out.println("split array: " + Arrays.toString(this.left));
        }
    }

    /**
     *
     * @param isLeftRoute boolean
     * @param aktPos int
     * @return int best choice for next destination
     */
    private int shortestDistance2(boolean isLeftRoute, int aktPos) {
        Arrays.fill(this.distance, 0);
        Deque<Double> smallest = new ArrayDeque<>(3);
        smallest.add(Double.MAX_VALUE);
        LinkedList<Integer> smallestPos = new LinkedList<>();
        smallestPos.add(aktPos);

        for (int i = 0; i < this.subsidiaries.size(); i++) {
            if (this.left[i] == isLeftRoute) {
                if (!this.previous.contains(this.subsidiaries.get(i))) {
                    this.distance[i] = this.subsidiaries.get(aktPos).getDistanceTo(this.subsidiaries.get(i));
                    if (this.distance[i] < smallest.getFirst()) {
                        smallest.addFirst(this.distance[i]);
                        smallestPos.addFirst(i);
                    }
                }
            }
        }
        Integer selectedSubsidiary = selectBestChoice(smallest, smallestPos);   //TODO: check if improvement necessary

        if (RoundtripFinderUtilities.debug())
            System.out.println(selectedSubsidiary + ", " + smallestPos);

        if (!this.previous.contains(this.subsidiaries.get(selectedSubsidiary))) {
            this.previous.add(this.subsidiaries.get(selectedSubsidiary));
            return shortestDistance2(isLeftRoute, selectedSubsidiary);
        }
        return selectedSubsidiary;
    }

    private Integer selectBestChoice(Deque<Double> smallest, LinkedList<Integer> smallestPos) {
        Integer selectedSubsidiary = smallestPos.getFirst();

        /*double quadraticSumDistanceSmallest = 0.0;
        double sumDistanceSmallest = 0.0;
        for (double d : smallest) {
            quadraticSumDistanceSmallest += (d * d);
            sumDistanceSmallest += d;
        }

        double standardDeviation = Math.sqrt(((quadraticSumDistanceSmallest - sumDistanceSmallest) / smallest.size()) / smallest.size());*/

        return selectedSubsidiary;
    }

    public void shortestDistance() {
        for (int j = 0; j < this.subsidiaries.size(); j++) {
            for (int i = 1; i < this.subsidiaries.size(); i++) {
                if (i != j) {
                    double direction = this.subsidiaries.get(j).getAngleTo(this.subsidiaries.get(i));
                    if (j != 0 && i != 1) { //not the first
                        if (Math.abs(this.direction[j] - direction) > ANGLE_DIFFERENCE    //not too close in direction
                                && Math.abs(this.direction[j] - direction) < 360 - ANGLE_DIFFERENCE) {
                            if (direction < this.distance[j]) {   //nearest
                                this.distance[j] = this.subsidiaries.get(j).getDistanceTo(this.subsidiaries.get(i));
                                this.direction[j] = direction;
                            }
                        }
                    } else {
                        this.distance[j] = this.subsidiaries.get(j).getDistanceTo(this.subsidiaries.get(i));
                        this.direction[j] = direction;
                    }
                }
            }
        }
    }

    public ArrayList<Subsidiary> getPath() {
        return this.previous;
    }

    public void printTotalLength() {
        System.out.println("\nTotal length: " + RoundtripFinderUtilities.length(this.previous));
    }

    public double getLength() {
        return RoundtripFinderUtilities.length(this.previous);
    }
}
