import org.jgrapht.graph.DefaultWeightedEdge;

public class SubsidiaryEdge extends DefaultWeightedEdge {
    protected double weight, direction;

    public SubsidiaryEdge(double weight, double direction) {
        this.weight = weight;
        this.direction = direction;
    }

    public double getDistance() {
        return weight;
    }

    public double getDirection() {
        return direction;
    }
}
