public class Subsidiary {

    static String[][] keys = {
            {"Nummer", "0"},
            {"msg Standort", "1"},
            {"Straße", "2"},
            {"Hausnummer", "3"},
            {"PLZ", "4"},
            {"Ort", "5"},
            {"Breitengrad", "6"},
            {"Längengrad", "7"}
    };

    String location, street, houseNr, city;
    int id, zip;
    double latitude, longitude;

    public Subsidiary(int id, String location, String street, String houseNr, int zip, String city, double latitude, double longitude) {
        this.id = id;
        this.location = location;
        this.street = street;
        this.houseNr = houseNr;
        this.city = city;
        this.zip = zip;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    /**
     * get Distance in km
     * @param to other Subsidiary: Subsidiary
     * @return distance: double
     */
    public double getDistanceTo(Subsidiary to) {
        if (to == this) {
            return 0.0;
        }
        double a2 = (this.latitude - to.latitude) * (this.latitude - to.latitude) * 71.44  * 71.44;
        double b2 = (this.longitude - to.longitude) * (this.longitude - to.longitude) * 111.13 * 111.13;
        return Math.sqrt(a2 + b2);
    }

    public double getAngleTo(Subsidiary to) {
        return Math.tan((this.longitude - to.longitude) / (this.latitude - to.latitude));
    }

    @Override
    public String toString() {
        return id + ", " + location + ", " + street + " " + houseNr + ", " + zip + " " + city + ", " + latitude + ", " + longitude;
    }
}
