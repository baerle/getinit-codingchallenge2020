import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultUndirectedGraph;

import java.util.List;

public class SubsidiaryGraph {
    Graph<Subsidiary, SubsidiaryEdge> subsidiaryGraph = new DefaultUndirectedGraph<>(SubsidiaryEdge.class);
    List<Subsidiary> subsidiaries;

    public SubsidiaryGraph(List<Subsidiary> subsidiaries) {
        this.subsidiaries = subsidiaries;
        for (Subsidiary subsidiary : this.subsidiaries) {
            this.subsidiaryGraph.addVertex(subsidiary);
        }
        this.calculateEdges();
    }

    private void calculateEdges() {
        for (Subsidiary fromSubsidiary : this.subsidiaries) {
            for (Subsidiary toSubsidiary : this.subsidiaries) {
                double distance = fromSubsidiary.getDistanceTo(toSubsidiary);
                double direction = fromSubsidiary.getAngleTo(toSubsidiary);
                this.subsidiaryGraph.addEdge(fromSubsidiary, toSubsidiary, new SubsidiaryEdge(distance, direction));
            }
        }
    }

    public Graph<Subsidiary, SubsidiaryEdge> getSubsidiaryGraph() {
        return subsidiaryGraph;
    }

    @Override
    public String toString() {
        return this.subsidiaryGraph.toString();
    }
}
