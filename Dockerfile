FROM openjdk:16-jdk-oracle
COPY . /usr/src/codeing
WORKDIR /usr/src/codeing/src/main/java
RUN javac -d ../../../target/classes Main.java FileInputReader.java RoundtripFinder.java RoundtripFinderImproved.java RoundtripFinderUtilities.java RoundtripFinderBranchAndBound.java
WORKDIR /usr/src/codeing
CMD ["java", "-cp", "./target/classes", "Main"]